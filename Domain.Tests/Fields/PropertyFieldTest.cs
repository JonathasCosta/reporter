﻿using NUnit.Framework;
using Reporter.Domain.Fields;

namespace Domain.Tests.Fields
{
    [TestFixture]
    public class PropertyFieldTest
    {
        [Test]
        public void CalculateRawValueFromBoxedProperty()
        {
            var field = new PropertyField("A.B");
            var obj = (object) new { A = (object) new { B = (object)123 } };
            var value = field.CalculateRawValue(obj);

            Assert.AreEqual(123, value);
        }

        [Test]
        public void CalculateRawValueFrom3rdLevelProperty()
        {
            var field = new PropertyField("A.B.C");
            var obj = new { A = new { B = new { C = "Test" } } };
            var value = field.CalculateRawValue(obj);

            Assert.AreEqual("Test", value);
        }
    }
}
