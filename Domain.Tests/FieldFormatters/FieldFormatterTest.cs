﻿using NUnit.Framework;
using Reporter.Domain.FieldFormatters;
using Reporter.Domain.Fields;

namespace Domain.Tests.FieldFormatters
{
    [TestFixture]
    public class FieldFormatterTest
    {
        [Test]
        public void RenderNullObject()
        {
            object value = null;
            var field = new EmptyField();
            var renderer = new FieldFormatter(field);
            field.CalculateRawValue(value);

            Assert.AreEqual(null, renderer.GetFormattedValue());
        }

        [Test]
        public void RenderNullString()
        {
            object value = (string)null;
            var field = new EmptyField();
            var renderer = new FieldFormatter(field);
            field.CalculateRawValue(value);

            Assert.AreEqual(null, renderer.GetFormattedValue());
        }

        [Test]
        public void RenderEmptyString()
        {
            var value = "";
            var field = new ConstantField(value);
            var renderer = new FieldFormatter(field);
            field.CalculateRawValue(value);

            Assert.AreEqual(value, renderer.GetFormattedValue());
        }

        [Test]
        public void RenderFormattedDouble()
        {
            var value = 123.123;
            var field = new ConstantField(value);
            field.CalculateRawValue(value);

            var renderer = new FieldFormatter(field) { Format = "F0" };
            Assert.AreEqual("123", renderer.GetFormattedValue());

            renderer.Format = "F1";
            Assert.AreEqual("123.1", renderer.GetFormattedValue());

            renderer.Format = "F2";
            Assert.AreEqual("123.12", renderer.GetFormattedValue());

            renderer.Format = "F3";
            Assert.AreEqual("123.123", renderer.GetFormattedValue());

            renderer.Format = "F4";
            Assert.AreEqual("123.1230", renderer.GetFormattedValue());
        }

        [Test]
        public void RenderFormattedTypedInteger()
        {
            var value = 123;
            var field = new ConstantField(value) { TypeName = "System.Int32" };
            field.CalculateRawValue(value);

            var renderer = new FieldFormatter(field) { Format = "0-0-0" };
            Assert.AreEqual("1-2-3", renderer.GetFormattedValue());
        }
    }
}
