﻿using System;
using Reporter.Domain.FieldFormatters;

namespace Reporter.Domain.Fields
{
    public interface IField
    {
        string Name { get; set; }
        string TypeName { get; set; }
        int? Precision { get; set; }
        bool IsNullable { get; set; }
        object DefaultValue { get; set; }
        object RawValue { get; set; }
        Template Template { get; set; }

        IFieldFormatter FieldFormatter { get; set; }

        bool HasType();
        Type GetFieldType();

        object GetFormattedValue(object rawData);
    }
}
