﻿using System;
using Reporter.Domain.FieldFormatters;

namespace Reporter.Domain.Fields
{
    public abstract class Field : IField
    {
        public virtual string Name { get; set; }
        public virtual string TypeName { get; set; }
        public virtual int? Precision { get; set; }
        public virtual bool IsNullable { get; set; }
        public virtual object DefaultValue { get; set; }
        public virtual object RawValue { get; set; }
        public virtual Template Template { get; set; }

        public virtual IFieldFormatter FieldFormatter { get; set; }

        public virtual bool HasType() => !string.IsNullOrWhiteSpace(TypeName);

        public virtual Type GetFieldType()
        {
            if (!HasType())
                throw new NullReferenceException("Field doesn't have a type name.");

            return Type.GetType(TypeName);
        }

        public abstract object CalculateRawValue(object rawData);

        public virtual object GetFormattedValue(object rawData)
        {
            if (CalculateRawValue(rawData) == null)
            {
                if (DefaultValue != null)
                    RawValue = DefaultValue;

                else if (!IsNullable)
                    throw new Exception($"The field '{Name}' does not allow null values and doesn't have a default value.");
            }

            if (FieldFormatter != null)
                return FieldFormatter.GetFormattedValue();

            return RawValue;
        }
    }
}
