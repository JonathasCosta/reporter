﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Reporter.Domain.Fields
{
    public class ConcatenatedField : Field
    {
        public virtual IList<IField> Fields { get; set; }

        public virtual string Separator { get; set; }

        public ConcatenatedField(string separator, IList<IField> fields)
        {
            Separator = separator;
            Fields = fields;
        }

        public override object CalculateRawValue(object rawData)
        {
            throw new NotImplementedException();
        }

        public override object GetFormattedValue(object rawData)
        {
            return string.Join(Separator ?? "", Fields.Select(f => f.GetFormattedValue(rawData)));
        }
    }
}