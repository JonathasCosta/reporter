﻿namespace Reporter.Domain.Fields
{
    public class AutoNumberField: Field
    {
        public virtual int? StartAt { get; set; }
        public virtual int? IncrementBy { get; set; }
        protected virtual int? Number { get; set; }

        public override object CalculateRawValue(object rawData)
        {
            if (Number == null)
                Number = StartAt ?? 1;
            else
                Number += IncrementBy ?? 1;

            RawValue = Number;

            return RawValue;
        }
    }
}
