﻿namespace Reporter.Domain.Fields
{
    public class EmptyField : Field
    {
        public override bool IsNullable => true;

        public override object CalculateRawValue(object rawData) => null;
    }
}
