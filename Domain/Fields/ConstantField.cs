﻿namespace Reporter.Domain.Fields
{
    public class ConstantField : Field
    {
        public ConstantField() { }
        public ConstantField(object constantValue) { DefaultValue = constantValue; }

        public override object CalculateRawValue(object rawData) => RawValue = DefaultValue;
    }
}
