﻿namespace Reporter.Domain.Fields
{
    public class PropertyField : Field
    {
        public virtual string PropertyName { get; set; }

        public PropertyField(string name)
        {
            PropertyName = name;
        }

        public override object CalculateRawValue(object rawData)
        {
            var properties = PropertyName.Split('.');
            var propertyType = rawData.GetType();
            var value = rawData;

            foreach (var propertyName in properties)
            {
                var property = propertyType.GetProperty(propertyName);
                value = property.GetValue(value);
                if (value != null)
                    propertyType = value.GetType();
            }

            RawValue = value;

            return RawValue;
        }
    }
}
