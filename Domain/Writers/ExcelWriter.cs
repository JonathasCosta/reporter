﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reporter.Domain.Writers
{
    public class ExcelWriter : BaseWriter<XElement, XElement>
    {
        protected override void Save(Stream stream)
        {
            using (var writer = new StreamWriter(stream))
            {
                writer.Write(Buffer.ToString());
            }
        }

        protected override void AppendItemToBuffer(XElement item)
        {
            Buffer.Add(item);
        }

        protected override void AppendValueToItem(XElement item, object value)
        {
            var cell = new XElement(XName.Get("td"));
            cell.Add(value);
            item.Add(cell);
        }

        protected override XElement CreateItem(Template template)
        {
            return new XElement(XName.Get("tr"));
        }

        protected override XElement CreateBuffer()
        {
            return new XElement(XName.Get("table"));
        }
    }
}
