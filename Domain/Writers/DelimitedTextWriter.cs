﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reporter.Domain.Writers
{
    public class DelimitedTextWriter : BaseTextWriter
    {
        public DelimitedTextWriter(string delimiter = ";") : base(delimiter) { }
    }
}
