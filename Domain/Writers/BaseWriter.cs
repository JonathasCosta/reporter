﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Reporter.Domain.Writers
{
    public abstract class BaseWriter<TItem, TBuffer>
    {
        protected virtual TBuffer Buffer { get; set; }

        public virtual void Write(Template template, Stream stream)
        {
            Buffer = CreateBuffer();
            WriteTemplate(template);
            Save(stream);
        }

        protected abstract void Save(Stream stream);

        protected virtual void WriteTemplate(Template template)
        {
            WriteTemplateHeader(template);

            if (template.Fields.Any())
            {
                if (template.DataSource == null && !template.IsDataNeeded())
                    template.DataSource = DataSource.Single();

                if (template.DataSource == null || template.DataSource.Count == 0)
                    throw new Exception("No data available!");

                WriteTemplateData(template);
            }

            WriteTemplateFooter(template);

            foreach (var childTemplate in template.Children)
                WriteTemplate(childTemplate);
        }

        protected virtual void WriteTemplateData(Template template)
        {
            foreach (var data in template.DataSource)
                WriteItemData(template, data);
        }

        protected virtual void WriteItemData(Template template, object data)
        {
            if (!template.Fields.Any())
                return;

            var item = CreateItem(template);
            WriteItemValues(template, data, item);
            AppendItemToBuffer(item);
        }

        protected virtual void WriteItemValues(Template template, object data, TItem item)
        {
            foreach (var field in template.Fields)
                AppendValueToItem(item, field.GetFormattedValue(data));
        }

        protected abstract void AppendItemToBuffer(TItem item);

        protected abstract void AppendValueToItem(TItem item, object value);

        protected abstract TItem CreateItem(Template template);

        protected abstract TBuffer CreateBuffer();

        protected virtual void WriteTemplateFooter(Template template) { }

        protected virtual void WriteTemplateHeader(Template template) { }
    }
}
