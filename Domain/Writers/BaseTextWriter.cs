﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reporter.Domain.Writers
{
    public abstract class BaseTextWriter : BaseWriter<IList<string>, IList<string>>
    {
        public string Delimiter { get; set; }

        public BaseTextWriter(string delimiter)
        {
            Delimiter = delimiter;
        }

        protected override void Save(Stream stream)
        {
            using (var writer = new StreamWriter(stream))
            {
                foreach (var line in Buffer)
                    writer.WriteLine(line);
            }
        }

        protected override void AppendItemToBuffer(IList<string> item)
        {
            Buffer.Add(string.Join(Delimiter, item));
        }

        protected override void AppendValueToItem(IList<string> item, object value)
        {
            item.Add(value?.ToString() ?? "");
        }

        protected override IList<string> CreateItem(Template template)
        {
            return new List<string>();
        }

        protected override IList<string> CreateBuffer()
        {
            return new List<string>();
        }
    }
}
