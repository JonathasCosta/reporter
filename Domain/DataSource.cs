﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Extensions;

namespace Reporter.Domain
{
    public class DataSource : ArrayList
    {
        public string Name { get; set; }
        public Type ItemType { get; }

        public DataSource(Type itemType)
            : this(itemType, new object[0])
        { }

        public DataSource(Type itemType, string name)
            : this(itemType, new object[0], name)
        { }

        public DataSource(Type itemType, IEnumerable collection, string name = null)
        {
            ItemType = itemType;
            foreach (var obj in collection)
                Add(obj);

            Name = name;
        }

        public static DataSource Single()
        {
            return new DataSource(typeof(int), new int[1]);
        }
        
        public LambdaExpression GetKeySelector(string keySelector)
        {
            var x = ItemType.ToParameterExpression("x");
            var member = keySelector.ToMemberExpression(x, false);

            return Expression.Lambda(member, x);
        }

        public DataSource InnerJoin(DataSource inner, string outerKeySelector, string innerKeySelector)
        {
            var oKey = GetKeySelector(outerKeySelector);
            var iKey = inner.GetKeySelector(innerKeySelector);

            return InnerJoin(inner, oKey, iKey);
        }

        public DataSource InnerJoin(DataSource inner, LambdaExpression outerKeySelector, LambdaExpression innerKeySelector)
        {
            if (inner?.ItemType == null || ItemType == null)
                return new DataSource(typeof(object));

            var resultSelector = GetJoinResultSelector(inner);

            return Join(inner, outerKeySelector, innerKeySelector, resultSelector);
        }

        private LambdaExpression GetJoinResultSelector(DataSource inner)
        {
            var outerParam = ItemType.ToParameterExpression("o");
            var innerParam = inner.ItemType.ToParameterExpression("i");
            var resultType = CreateJoinResultType(inner);

            var bindings = resultType.GetProperties().Select(x => IEnumerableExtensions.BindToParam(x, outerParam, innerParam)).ToArray();
            var initializator = Expression.MemberInit(Expression.New(resultType), bindings);

            return Expression.Lambda(initializator, outerParam, innerParam);
        }

        private Type CreateJoinResultType(DataSource inner)
        {
            var classEmitter = new ClassEmitter();

            if (ItemType.IsJoinResultEntityType())
                classEmitter.CopyProperties(ItemType);
            else
                classEmitter.CreateProperty(Name ?? ItemType.Name, ItemType);

            classEmitter.CreateProperty(inner.Name ?? inner.ItemType.Name, inner.ItemType);

            return classEmitter.CreateType();
        }

        public DataSource Join(DataSource inner, LambdaExpression outerKeySelector, LambdaExpression innerKeySelector, LambdaExpression resultSelector)
        {
            var method = typeof(Enumerable).GetMethodInfo("Join", new[] { typeof(IEnumerable<>), typeof(IEnumerable<>), typeof(Func<,>), typeof(Func<,>), typeof(Func<,,>) });
            var outerKey = outerKeySelector.Compile();
            var innerKey = innerKeySelector.Compile();
            var resultObj = resultSelector.Compile();
            var methodInstance = method.MakeGenericMethod(ItemType, inner.ItemType, innerKeySelector.ReturnType, resultSelector.ReturnType);
            var result = methodInstance.Invoke(null, new object[] { CastToGeneric(), inner.CastToGeneric(), outerKey, innerKey, resultObj });

            return new DataSource(resultSelector.ReturnType, result as IEnumerable);
        }

        private object CastToGeneric()
        {
            var method = typeof(Enumerable).GetMethodInfo("Cast", new[] { typeof(IEnumerable) });
            var methodInstance = method.MakeGenericMethod(ItemType);
            var result = methodInstance.Invoke(null, new object[] {this});

            return result;
        }
    }
}
