﻿using System.Collections.Generic;
using System.Linq;
using Reporter.Domain.Fields;

namespace Reporter.Domain
{
    public class Template
    {
        public Template Parent { get; private set; }

        public IList<Template> Children { get; }
        public IList<IField> Fields { get; }
        public DataSource DataSource { get; set; }

        public Template()
        {
            Children = new List<Template>();
            Fields = new List<IField>();
        }

        public void AddField(IField field)
        {
            Fields.Add(field);
            field.Template = this;
        }

        public void SetParent(Template parent)
        {
            Parent = parent;
            Parent.Children.Add(this);
        }

        public bool IsDataNeeded()
        {
            return Fields.Any(field => field is PropertyField);
        }
    }
}
