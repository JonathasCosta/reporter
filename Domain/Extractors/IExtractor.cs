﻿namespace Reporter.Domain.Extractors
{
    public interface IExtractor
    {
        DataSource Extract();
    }
}
