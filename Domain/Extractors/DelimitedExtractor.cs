﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Extensions;
using Reporter.Domain.FieldFormatters;
using Reporter.Domain.Fields;

namespace Reporter.Domain.Extractors
{
    public class DelimitedExtractor : Extractor
    {
        public virtual string Delimiter { get; set; }
        public virtual string Filename { get; set; }
        public virtual Template Template { get; set; }

        public DelimitedExtractor(string filename, Template template, string dataSourceName = null, string delimiter = ";")
        {
            var assemblyEmitter = new AssemblyEmitter(filename);
            var classEmitter = new ClassEmitter(NameGenerator.CreateName(), assemblyEmitter);
            foreach (var field in template.Fields)
            {
                var name = GetPropertyNameFromField(field);
                var type = field.HasType() ? field.GetFieldType() : typeof(string);
                classEmitter.CreateProperty(name, type, field.IsNullable);
            }

            Delimiter = delimiter;
            ExtractedType = classEmitter.CreateType();
            Filename = filename;
            DataSourceName = dataSourceName;
            Template = template;
        }

        public override DataSource Extract()
        {
            var collection = new DataSource(ExtractedType, DataSourceName);

            using (var file = new FileStream(Filename, FileMode.Open))
            {
                using (var reader = new StreamReader(file))
                {
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        if (string.IsNullOrEmpty(line))
                            break;

                        var fields = Regex.Split(line, Delimiter);
                        var item = CreateItem();

                        for (var i = 0; i < Template.Fields.Count; i++)
                        {
                            var field = Template.Fields[i];
                            var propertyName = GetPropertyNameFromField(field);
                            var textValue = fields[i];
                            var propertyValue = string.IsNullOrEmpty(textValue) ? null : Convert.ChangeType(textValue, field.GetFieldType());

                            item.SetPropertyValue(propertyName, propertyValue);
                        }

                        collection.Add(item);
                    }
                }
            }

            return collection;
        }

        private static string GetPropertyNameFromField(IField field)
        {
            var propertyName = field.Name;
            if (propertyName == null && field is PropertyField)
                propertyName = (field as PropertyField).PropertyName;
            if (propertyName == null)
                throw new NullReferenceException("Field name cannot be null.");
            propertyName = propertyName.Replace(".", "_");
            return propertyName;
        }
    }
}
