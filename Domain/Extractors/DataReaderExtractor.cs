﻿using System;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using Extensions;

namespace Reporter.Domain.Extractors
{
    public class DataReaderExtractor : Extractor
    {
        public virtual string ProviderName { get; set; }
        public virtual string ConnectionString { get; set; }
        public virtual string Sql { get; set; }
        public string DataSourceName { get; set; }
        public virtual string HashValue { get; set; }

        public DataReaderExtractor(string providerName, string connectionString, string sql, string datasourceName = null)
        {
            ProviderName = providerName;
            ConnectionString = connectionString;
            Sql = sql;
            DataSourceName = datasourceName;

            HashValue = NameGenerator.CreateName(providerName, connectionString, sql);

            try
            {
                LoadExtractedType();
            }
            catch (Exception)
            {
                ExtractType();
            }
        }

        private void ExtractType()
        {
            UseCommand(command =>
            {
                var assemblyEmitter = new AssemblyEmitter(ProviderName, ConnectionString, Sql);
                var classEmitter = new SchemaTableClassEmitter(command, assemblyEmitter);
                ExtractedType = classEmitter.CreateType();
                assemblyEmitter.Save();
            });
        }

        private void LoadExtractedType()
        {
            var assembly = AppDomain.CurrentDomain.GetAssemblies().SingleOrDefault(a => a.GetName().Name == HashValue);
            if (assembly != null)
            {
                ExtractedType = assembly.GetExportedTypes()[0];
                return;
            }

            assembly = AppDomain.CurrentDomain.Load(new AssemblyName(HashValue));
            ExtractedType = assembly.GetExportedTypes()[0];
        }

        public override DataSource Extract()
        {
            return UseCommand(command =>
            {
                var reader = command.ExecuteReader();
                var collection = new DataSource(ExtractedType, DataSourceName);

                while (reader.Read())
                {
                    var item = CreateItem();

                    for (var i = 0; i < reader.FieldCount; i++)
                        item.SetPropertyValue(reader.GetName(i), reader.GetValue(reader.GetName(i)));

                    collection.Add(item);
                }

                return collection;
            });
        }

        protected virtual void UseCommand(Action<IDbCommand> action)
        {
            using (var command = PrepareCommand())
            {
                try
                {
                    command.Connection.Open();

                    action(command);
                }
                finally
                {
                    if (command.Connection.State == ConnectionState.Open)
                        command.Connection.Close();
                }
            }
        }

        protected virtual TResult UseCommand<TResult>(Func<IDbCommand, TResult> func)
        {
            using (var command = PrepareCommand())
            {
                try
                {
                    command.Connection.Open();

                    return func(command);
                }
                finally
                {
                    if (command.Connection.State == ConnectionState.Open)
                        command.Connection.Close();
                }
            }
        }

        protected virtual IDbCommand PrepareCommand()
        {
            var providerFactory = DbProviderFactories.GetFactory(ProviderName);

            var connection = providerFactory.CreateConnection();
            if (connection == null)
                throw new Exception("It was not possible to create a connection.");

            connection.ConnectionString = ConnectionString;

            var command = connection.CreateCommand();
            command.CommandText = Sql;

            return command;
        }
    }
}
