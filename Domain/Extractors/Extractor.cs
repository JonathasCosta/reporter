﻿using System;

namespace Reporter.Domain.Extractors
{
    public abstract class Extractor : IExtractor
    {
        protected virtual Type ExtractedType { get; set; }
        protected virtual Type CollectionType { get; set; }
        public virtual string DataSourceName { get; set; }

        public abstract DataSource Extract();

        protected virtual void SetProperty(string name, object value, object instance)
        {
            ExtractedType.GetProperty(name).SetValue(instance, value);
        }
        
        protected virtual object CreateItem()
        {
            return Activator.CreateInstance(ExtractedType);
        }
    }
}
