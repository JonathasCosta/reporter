﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Extensions;
using Reporter.Domain.FieldFormatters;
using Reporter.Domain.Fields;
using Reporter.Domain.Writers;

namespace Reporter.Domain.Extractors
{
    public class FixedLengthExtractor : Extractor
    {
        public virtual string Filename { get; set; }
        public virtual Template Template { get; set; }

        public FixedLengthExtractor(string filename, Template template, string dataSourceName = null)
        {
            var assemblyEmitter = new AssemblyEmitter(filename);
            var classEmitter = new ClassEmitter(NameGenerator.CreateName(), assemblyEmitter);
            foreach (var field in template.Fields)
            {
                var name = field.Name;
                if (name == null && field is PropertyField)
                    name = (field as PropertyField).PropertyName;
                if (name == null)
                    throw new NullReferenceException("Field name cannot be null.");

                name = name.Replace(".", "_");

                var type = field.HasType() ? field.GetFieldType() : typeof(string);
                classEmitter.CreateProperty(name, type, field.IsNullable);
            }

            ExtractedType = classEmitter.CreateType();
            Filename = filename;
            DataSourceName = dataSourceName;
            Template = template;
        }

        public override DataSource Extract()
        {
            var collection = new DataSource(ExtractedType, DataSourceName);

            using (var file = new FileStream(Filename, FileMode.Open))
            {
                using (var reader = new StreamReader(file))
                {
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        if (string.IsNullOrEmpty(line))
                            break;

                        var item = CreateItem();

                        for (var i = 0; i < Template.Fields.Count; i++)
                        {
                            var field = Template.Fields[i];
                            var propertyName = field.Name;
                            if (propertyName == null && field is PropertyField)
                                propertyName = (field as PropertyField).PropertyName;
                            if (propertyName == null)
                                throw new NullReferenceException("Field name cannot be null.");
                            propertyName = propertyName.Replace(".", "_");
                            var size = (field.FieldFormatter as FixedLengthFieldFormatter).Size.Value;
                            var textValue = line.Substring(0, size).Trim();
                            var propertyValue = string.IsNullOrEmpty(textValue) ? null : Convert.ChangeType(textValue, field.GetFieldType());

                            item.SetPropertyValue(propertyName, propertyValue);

                            line = line.Substring(size);
                        }

                        collection.Add(item);
                    }
                }
            }

            return collection;
        }
    }
}
