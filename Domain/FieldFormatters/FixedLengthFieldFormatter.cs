﻿using System;
using Reporter.Domain.Fields;

namespace Reporter.Domain.FieldFormatters
{
    public class FixedLengthFieldFormatter : FieldFormatter
    {
        public virtual char? EmptyChar { get; set; }
        public virtual int? Size { get; set; }
        public virtual AlignMode AlignMode { get; set; }

        public const char DEFAULT_EMPTY_CHAR = ' ';

        public FixedLengthFieldFormatter(IField field, int size, AlignMode alignMode = AlignMode.Left) : this(field, size, DEFAULT_EMPTY_CHAR, AlignMode.Left)
        {
        }

        public FixedLengthFieldFormatter(IField field, int size, char emptyChar = DEFAULT_EMPTY_CHAR, AlignMode alignMode = AlignMode.Left) : base(field)
        {
            Size = size;
            AlignMode = alignMode;
        }

        public override object GetFormattedValue()
        {
            if (Size == null)
                throw new ArgumentNullException(nameof(Size));

            var value = (base.GetFormattedValue() ?? "").ToString();
            var padChar = EmptyChar ?? DEFAULT_EMPTY_CHAR;

            switch (AlignMode)
            {
                case AlignMode.Left:
                    value = value.PadRight(Size.Value, padChar);
                    break;

                case AlignMode.Center:
                    var spaces = Size.Value - value.Length;
                    var padLeft = spaces / 2 + value.Length;
                    value = value.PadLeft(padLeft, padChar).PadRight(Size.Value, padChar);
                    break;

                case AlignMode.Right:
                    value = value.PadLeft(Size.Value, padChar);
                    break;
            }

            return value;
        }
    }

    public enum AlignMode
    {
        Left,
        Center,
        Right
    }
}
