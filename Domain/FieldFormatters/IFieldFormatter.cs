﻿using Reporter.Domain.Fields;

namespace Reporter.Domain.FieldFormatters
{
    public interface IFieldFormatter
    {
        string Format { get; set; }
        string CultureName { get; set; }
        IField Field { get; set; }

        object GetFormattedValue();
    }
}
