﻿using System;
using System.Globalization;
using Reporter.Domain.Fields;

namespace Reporter.Domain.FieldFormatters
{
    public class FieldFormatter : IFieldFormatter
    {
        public virtual string Format { get; set; }
        public virtual string CultureName { get; set; }
        public virtual IField Field { get; set; }

        public FieldFormatter(IField field)
        {
            Field = field;
            Field.FieldFormatter = this;
        }

        public virtual object GetFormattedValue()
        {
            var value = Field.RawValue;

            if (value == null)
                return null;

            if ((value is string) && string.IsNullOrEmpty((string)value))
                return value;

            if (Field.HasType())
                value = Convert.ChangeType(value, Field.GetFieldType(), GetCulture());

            if (!string.IsNullOrEmpty(Format))
                value = string.Format(GetCulture(), Format, value);

            return value;
        }

        protected virtual CultureInfo GetCulture()
        {
            return string.IsNullOrEmpty(CultureName) ? CultureInfo.CurrentCulture : CultureInfo.GetCultureInfo(CultureName);
        }
    }
}
