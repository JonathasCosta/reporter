﻿using Extensions;
using Reporter.Domain.Fields;

namespace Reporter.Domain.FieldFormatters
{
    public class TSqlFieldFormatter : FieldFormatter
    {
        public TSqlFieldFormatter(IField field) : base(field)
        {
            CultureName = "en-us";
        }

        public override object GetFormattedValue()
        {
            if (Field.IsNullable && Field.RawValue == null)
                return "NULL";
            
            switch (Field.TypeName)
            {
                case TypeExtensions.String:
                    Format = "'{0}'";
                    break;

                case TypeExtensions.Double:
                case TypeExtensions.Single:
                    CultureName = "en-us";
                    Format = "G";
                    break;

                case TypeExtensions.DateTime:
                    Format = "CONVERT(DATETIME, '{0:yyyy/MM/dd/ hh:mm:ss}')";
                    break;
            }

            return base.GetFormattedValue();
        }
    }
}
