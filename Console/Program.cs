﻿using System.Collections.Generic;
using System.Configuration;
using System.IO;
using Extensions;
using Reporter.Domain;
using Reporter.Domain.Extractors;
using Reporter.Domain.FieldFormatters;
using Reporter.Domain.Fields;
using Reporter.Domain.Writers;

namespace Console
{
    class Program
    {
        private static string fixedLengthFile = "contribuintes.txt";
        private static string delimitedFile = "contribuintes.csv";
        private static string scriptFile = "contribuintes.sql";
        private static string excelFile = "contribuintes.htm";

        static void Main(string[] args)
        {
            // 1. Get data from db
            var settings = ConfigurationManager.ConnectionStrings["SISSMO-SqlServer"];
            var contribuinteExtractor = new DataReaderExtractor(settings.ProviderName, settings.ConnectionString, "SELECT * FROM dbo.Contribuinte", "ContribuintesDb");
            var contribuintesDb = contribuinteExtractor.Extract();

            // 2. Convert data to fixed length file
            getFixedLength(contribuintesDb);

            // 3. Read data from fixed length file
            var fixedLengthExtractor = new FixedLengthExtractor(fixedLengthFile, getFixedLengthTemplate(null), "ContribuintesFixed");
            var contribuintesFixed = fixedLengthExtractor.Extract();

            // 4. Convert data to delimited file
            getDelimited(contribuintesFixed);

            // 5. Read data from delimited file
            var delimitedExtractor = new DelimitedExtractor(delimitedFile, getDelimitedTemplate(null), "ContribuintesDelimited");
            var contribuintesDelimited = delimitedExtractor.Extract();

            // 6. Convert data to sql script
            getScript(contribuintesDelimited);

            // 7. Convert data to Excel
            getExcel(contribuintesDelimited);
        }

        private static void getDelimited(DataSource datasource)
        {
            var root = getDelimitedTemplate(datasource);
            using (var file = new FileStream(delimitedFile, FileMode.Create))
            {
                var writer = new DelimitedTextWriter();
                writer.Write(root, file);
            }
        }

        private static Template getDelimitedTemplate(DataSource datasource)
        {
            var root = new Template { DataSource = datasource };

            root.AddField(new FieldFormatter(new PropertyField("Id") { TypeName = TypeExtensions.Int32 }).Field);
            root.AddField(new FieldFormatter(new PropertyField("Nome") { TypeName = TypeExtensions.String }).Field);
            root.AddField(new FieldFormatter(new PropertyField("Numero") { TypeName = TypeExtensions.String }).Field);
            root.AddField(new FieldFormatter(new PropertyField("DataCadastro") { IsNullable = true, TypeName = TypeExtensions.DateTime }) { Format = "{0:dd/MM/yyyy}", CultureName = "pt-br" }.Field);
            root.AddField(new FieldFormatter(new PropertyField("NumeroEstabelecimento") { IsNullable = true, TypeName = TypeExtensions.Int32 }).Field);
            root.AddField(new FieldFormatter(new PropertyField("NumeroContribuinte") { IsNullable = true, TypeName = TypeExtensions.Int64 }).Field);

            return root;
        }

        private static void getFixedLength(DataSource declaracoesDeContribuintes)
        {
            var root = getFixedLengthTemplate(declaracoesDeContribuintes);
            using (var file = new FileStream(fixedLengthFile, FileMode.Create))
            {
                var writer = new FixedLengthTextWriter();
                writer.Write(root, file);
            }
        }

        private static Template getFixedLengthTemplate(DataSource declaracoesDeContribuintes)
        {
            var root = new Template { DataSource = declaracoesDeContribuintes };

            root.AddField(new FixedLengthFieldFormatter(new PropertyField("Id") { TypeName = TypeExtensions.Int32 }, 11, AlignMode.Right).Field);
            root.AddField(new FixedLengthFieldFormatter(new PropertyField("Nome") { TypeName = TypeExtensions.String }, 251, AlignMode.Left).Field);
            root.AddField(new FixedLengthFieldFormatter(new PropertyField("Numero") { TypeName = TypeExtensions.String }, 21, AlignMode.Left).Field);
            root.AddField(new FixedLengthFieldFormatter(new PropertyField("DataCadastro") { IsNullable = true, TypeName = TypeExtensions.DateTime }, 11, AlignMode.Right) { Format = "{0:dd/MM/yyyy}", CultureName = "pt-br" }.Field);
            root.AddField(new FixedLengthFieldFormatter(new PropertyField("NumeroEstabelecimento") { IsNullable = true, TypeName = TypeExtensions.Int32 }, 11, AlignMode.Right).Field);
            root.AddField(new FixedLengthFieldFormatter(new PropertyField("NumeroContribuinte") { IsNullable = true, TypeName = TypeExtensions.Int64 }, 22, AlignMode.Right).Field);

            return root;
        }
        
        private static void getScript(DataSource datasource)
        {
            var root = getSqlTemplate(datasource);
            using (var file = new FileStream(scriptFile, FileMode.Create))
            {
                var writer = new DelimitedTextWriter("");
                writer.Write(root, file);
            }
        }

        private static Template getSqlTemplateUsingConcatenatedField(DataSource declaracoesDeContribuintes)
        {
            var root = new Template();

            var insertContribuintesComment = new Template();
            insertContribuintesComment.AddField(new ConstantField("-- INSERÇÃO DE CONTRIBUINTES"));
            insertContribuintesComment.SetParent(root);

            var insertContribuintes = new Template { DataSource = declaracoesDeContribuintes };
            insertContribuintes.AddField(new ConstantField("INSERT INTO CONTRIBUINTE (ID, NOME, NUMERO, DATACADASTRO, NUMEROESTABELECIMENTO, NUMEROCONTRIBUINTE) VALUES ("));
            insertContribuintes.AddField(new ConcatenatedField(", ", new[]
            {
                new PropertyField("Id"){ TypeName = TypeExtensions.Int32 },
                new TSqlFieldFormatter(new PropertyField("Nome") { TypeName = TypeExtensions.String }).Field,
                new TSqlFieldFormatter(new PropertyField("Numero") { TypeName = TypeExtensions.String }).Field,
                new TSqlFieldFormatter(new PropertyField("DataCadastro") { IsNullable = true, TypeName = TypeExtensions.DateTime }).Field,
                new TSqlFieldFormatter(new PropertyField("NumeroEstabelecimento") { IsNullable = true, TypeName = TypeExtensions.Int32 }).Field,
                new TSqlFieldFormatter(new PropertyField("NumeroContribuinte") { IsNullable = true, TypeName = TypeExtensions.Int64 }).Field
            }));
            insertContribuintes.AddField(new ConstantField(");"));
            insertContribuintes.SetParent(root);

            return root;
        }

        private static Template getSqlTemplate(DataSource declaracoesDeContribuintes)
        {
            var root = new Template();

            var insertContribuintesComment = new Template();
            insertContribuintesComment.AddField(new ConstantField("-- INSERÇÃO DE CONTRIBUINTES"));
            insertContribuintesComment.SetParent(root);

            var insertContribuintes = new Template { DataSource = declaracoesDeContribuintes };
            insertContribuintes.AddField(new ConstantField("INSERT INTO CONTRIBUINTE (ID, NOME, NUMERO, DATACADASTRO, NUMEROESTABELECIMENTO, NUMEROCONTRIBUINTE) VALUES ("));
            insertContribuintes.AddField(new PropertyField("Id") { TypeName = TypeExtensions.Int32 });
            insertContribuintes.AddField(new ConstantField(", "));
            insertContribuintes.AddField(new TSqlFieldFormatter(new PropertyField("Nome") { TypeName = TypeExtensions.String }).Field);
            insertContribuintes.AddField(new ConstantField(", "));
            insertContribuintes.AddField(new TSqlFieldFormatter(new PropertyField("Numero") { TypeName = TypeExtensions.String }).Field);
            insertContribuintes.AddField(new ConstantField(", "));
            insertContribuintes.AddField(new TSqlFieldFormatter(new PropertyField("DataCadastro") { IsNullable = true, TypeName = TypeExtensions.DateTime }).Field);
            insertContribuintes.AddField(new ConstantField(", "));
            insertContribuintes.AddField(new TSqlFieldFormatter(new PropertyField("NumeroEstabelecimento") { IsNullable = true, TypeName = TypeExtensions.Int32 }).Field);
            insertContribuintes.AddField(new ConstantField(", "));
            insertContribuintes.AddField(new TSqlFieldFormatter(new PropertyField("NumeroContribuinte") { IsNullable = true, TypeName = TypeExtensions.Int64 }).Field);
            insertContribuintes.AddField(new ConstantField(");"));
            insertContribuintes.SetParent(root);

            return root;
        }

        private static void getExcel(DataSource datasource)
        {
            var root = getDelimitedTemplate(datasource);
            using (var file = new FileStream(excelFile, FileMode.Create))
            {
                var writer = new ExcelWriter();
                writer.Write(root, file);
            }
        }
    }
}

