﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Extensions
{
    public static class TypeExtensions
    {
        public const string String = "System.String, mscorlib";
        public const string DateTime = "System.DateTime, mscorlib";
        public const string Int64 = "System.String, mscorlib";
        public const string Int32 = "System.Int32, mscorlib";
        public const string Int16 = "System.Int16, mscorlib";
        public const string Double = "System.Double, mscorlib";
        public const string Single = "System.Single, mscorlib";

        public static MethodInfo GetMethodInfo(this Type type, string name, Type[] arguments)
        {
            var members = type.GetMember(name);
            foreach (var member in members)
            {
                var parameters = ((MethodInfo) member).GetParameters();
                if (parameters.Length != arguments.Length)
                    continue;

                var parameterTypes = parameters.Select(p => p.ParameterType.IsGenericType ? p.ParameterType.GetGenericTypeDefinition() : p.ParameterType);
                var argumentTypes = arguments.Select(a => a.IsGenericType ? a.GetGenericTypeDefinition() : a);

                if (parameterTypes.SequenceEqual(argumentTypes))
                    return member as MethodInfo;
            }

            throw new Exception("Não foi encontrado nenhum método que satisfizesse o critério de parâmetros");
        }

        public static string GetSafeAssemblyQualifiedName(this Type type)
        {
            if (type.IsGenericType)
            {
                // ReSharper disable once PossibleNullReferenceException
                var parts = type.GetGenericTypeDefinition().AssemblyQualifiedName.Split(',');
                parts[0] = string.Format("{0}[[{1}]]", parts[0], string.Join(",", type.GetGenericArguments().Select(t => t.GetSafeAssemblyQualifiedName()).ToArray()));

                return string.Join(",", parts.Take(2));
            }

            if (!string.IsNullOrWhiteSpace(type.FullName))
                return string.Join(",", type.AssemblyQualifiedName.Split(',').Take(2));

            return string.Empty;
        }

        public static object CreateInstance(this Type type)
        {
            return Activator.CreateInstance(type);
        }

        public static object CreateInstance(this Type type, params object[] args)
        {
            return Activator.CreateInstance(type, args);
        }

        public static bool IsJoinResultEntityType(this Type type)
        {
            return !type.GetProperties().Any(x => x.PropertyType == typeof(string) || x.PropertyType.IsValueType);
        }

        public static ParameterExpression ToParameterExpression(this Type type, string parameterName)
        {
            return Expression.Parameter(type, parameterName);
        }
    }
}
