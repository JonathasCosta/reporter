﻿namespace Extensions
{
    public static class ReflectionExtensions
    {
        public static void SetPropertyValue(this object obj, string propertyName, object value)
        {
            obj.GetType().GetProperty(propertyName).SetValue(obj, value);
        }
    }
}
