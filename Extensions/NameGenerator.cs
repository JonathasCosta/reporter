﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;

namespace Extensions
{
    public static class NameGenerator
    {
        public static string CreateName()
        {
            return CreateName(DateTime.UtcNow);
        }

        public static string CreateName(params object[] seed)
        {
            if (seed.Length == 0)
                return CreateName();

            var formatter = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                formatter.Serialize(ms, seed);

                var hasher = HashAlgorithm.Create();
                var hashValue = hasher.ComputeHash(ms.ToArray());
                return hashValue.ToBase37(); // Convert.ToBase64String(hashValue);
            }
        }

        public static string ToUnderscoreLoweredCamelCase(this string name)
        {
            return "_" + char.ToLower(name[0]) + name.Substring(1);
        }

        public static string ToBase37(this byte[] bytes)
        {
            var charBase = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_";
            var result = "";

            for (int i = 0; i < bytes.Length; i++)
            {
                var value = bytes[i];
                do
                {
                    result = charBase[value%charBase.Length] + result;
                } while ((value /= 36) > 0);
            }

            return result;
        }
    }
}
