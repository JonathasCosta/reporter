﻿using System;
using System.Data;

namespace Extensions
{
    public static class IDataReaderExtensions
    {
        public static object GetValue(this IDataReader reader, string name)
        {
            return reader[name] is DBNull ? null : reader[name];
        }
    }
}
