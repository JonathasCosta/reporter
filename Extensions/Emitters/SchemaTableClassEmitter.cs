﻿using System;
using System.Data;

namespace Extensions
{
    public class SchemaTableClassEmitter : ClassEmitter
    {
        public SchemaTableClassEmitter(IDbCommand command, AssemblyEmitter assemblyEmitter)
            : this(command.ExecuteReader(CommandBehavior.SchemaOnly), assemblyEmitter)
        {

        }

        public SchemaTableClassEmitter(IDataReader reader, AssemblyEmitter assemblyEmitter)
            : this(reader.GetSchemaTable(), NameGenerator.CreateName(), assemblyEmitter)
        {

        }

        public SchemaTableClassEmitter(DataTable schemaTable, string className, AssemblyEmitter assemblyEmitter)
            : base(className, assemblyEmitter)
        {
            foreach (DataRow row in schemaTable.Rows)
                CreateProperty((string)row["ColumnName"], (Type)row["DataType"], (bool)row["AllowDBNull"]);
        }
    }
}
