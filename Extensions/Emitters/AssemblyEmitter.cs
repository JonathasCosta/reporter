﻿using System;
using System.Reflection;
using System.Reflection.Emit;

namespace Extensions
{
    public sealed class AssemblyEmitter
    {
        private AssemblyBuilder AssemblyBuilder { get; set; }
        public ModuleBuilder ModuleBuilder { get; private set; }

        public AssemblyEmitter(params object[] seed)
            : this(NameGenerator.CreateName(seed))
        {
        }

        private AssemblyEmitter(string name)
        {
            var assemblyName = new AssemblyName(name)
            {
                Version = new Version(DateTime.UtcNow.ToString("yyyy.M.d.hmm"))
            };
            
            AssemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.RunAndSave);
            ModuleBuilder = AssemblyBuilder.DefineDynamicModule(assemblyName.Name, $"{assemblyName.Name}.dll");
        }

        public void Save()
        {
            AssemblyBuilder.Save($"{AssemblyBuilder.GetName().Name}.dll");
        }
    }
}