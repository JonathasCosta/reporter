﻿using System;
using System.Reflection;
using System.Reflection.Emit;

namespace Extensions
{
    public class ClassEmitter
    {
        protected AssemblyEmitter AssemblyEmitter { get; set; }
        public TypeBuilder TypeBuilder { get; protected set; }

        public ClassEmitter()
            : this(NameGenerator.CreateName(), new AssemblyEmitter())
        {
        }

        public  ClassEmitter(string className, AssemblyEmitter assemblyEmitter)
        {
            AssemblyEmitter = assemblyEmitter;
            TypeBuilder = AssemblyEmitter.ModuleBuilder.DefineType(className, TypeAttributes.Public);
        }

        public void CreateProperty(string name, Type type)
        {
            PropertyEmitter.Create(this, name, type);
        }

        public void CreateProperty(string name, Type type, bool nullable)
        {
            if (nullable && type.IsValueType)
            {
                CreateProperty(name, typeof(Nullable<>).MakeGenericType(type));
                return;
            }

            CreateProperty(name, type);
        }

        public void CopyProperties(Type type)
        {
            foreach (var p in type.GetProperties())
                CreateProperty(p.PropertyType.Name, p.PropertyType);
        }

        public Type CreateType()
        {
            return TypeBuilder.CreateType();
        }
    }
}
