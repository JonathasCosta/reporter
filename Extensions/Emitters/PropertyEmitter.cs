﻿using System;
using System.Reflection;
using System.Reflection.Emit;

namespace Extensions
{
    public sealed class PropertyEmitter
    {
        public PropertyEmitter(ClassEmitter classEmitter, string name, Type type)
        {
            var fieldBuilder = classEmitter.TypeBuilder.DefineField(name.ToUnderscoreLoweredCamelCase(), type, FieldAttributes.Private);
            var propertyBuilder = classEmitter.TypeBuilder.DefineProperty(name, PropertyAttributes.None, type, null);
            var getBuilder = classEmitter.TypeBuilder.DefineMethod("get_" + name, MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig, type, null);
            var setBuilder = classEmitter.TypeBuilder.DefineMethod("set_" + name, MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig, null, new[] { type });

            var get = getBuilder.GetILGenerator();
            get.Emit(OpCodes.Ldarg_0);
            get.Emit(OpCodes.Ldfld, fieldBuilder);
            get.Emit(OpCodes.Ret);

            var set = setBuilder.GetILGenerator();
            set.Emit(OpCodes.Ldarg_0);
            set.Emit(OpCodes.Ldarg_1);
            set.Emit(OpCodes.Stfld, fieldBuilder);
            set.Emit(OpCodes.Ret);

            propertyBuilder.SetGetMethod(getBuilder);
            propertyBuilder.SetSetMethod(setBuilder);
        }

        public static PropertyEmitter Create(ClassEmitter classEmitter, string name, Type type)
        {
            return new PropertyEmitter(classEmitter, name, type);
        }
    }
}
