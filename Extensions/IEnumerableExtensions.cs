﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Extensions
{
    public static class IEnumerableExtensions
    {
        public static Type GetItemType(this IEnumerable collection)
        {
            if (!collection.GetType().IsJoinResultEntityType())
                return collection.GetType().GenericTypeArguments[0];

            var enumerator = collection.GetEnumerator();
            if (enumerator == null)
                throw new NotImplementedException();

            if (enumerator.MoveNext())
                return enumerator.Current.GetType();

            throw new NotImplementedException();
        }

        private static LambdaExpression GetKeySelector(Type keyType, string keySelector)
        {
            var x = keyType.ToParameterExpression("x");
            var member = keySelector.ToMemberExpression(x, false);

            return Expression.Lambda(member, x);
        }

        public static IEnumerable InnerJoin(this IEnumerable outer, IEnumerable inner, string outerKeySelector, string innerKeySelector)
        {
            var oKey = GetKeySelector(outer.GetItemType(), outerKeySelector);
            var iKey = GetKeySelector(inner.GetItemType(), innerKeySelector);

            Type resultType;
            var result = outer.InnerJoin(inner, oKey, iKey, out resultType);

            return result;
        }

        private static Type CreateResultType(Type outerType, Type innerType)
        {
            var classEmitter = new ClassEmitter();

            if (!outerType.IsJoinResultEntityType())
            {
                classEmitter.CreateProperty(outerType.Name, outerType);
            }
            else
            {
                foreach (var p in outerType.GetProperties())
                    classEmitter.CreateProperty(p.PropertyType.Name, p.PropertyType);
            }

            classEmitter.CreateProperty(innerType.Name, innerType);

            var resultType = classEmitter.CreateType();
            return resultType;
        }

        public static MemberAssignment BindToParam(PropertyInfo property, ParameterExpression outerParam, ParameterExpression innerParam)
        {
            if (property.PropertyType == innerParam.Type)
                return Expression.Bind(property, innerParam);

            if (property.PropertyType == outerParam.Type)
                return Expression.Bind(property, outerParam);

            foreach (var p in outerParam.Type.GetProperties())
            {
                if (p.PropertyType == property.PropertyType)
                    return Expression.Bind(property, Expression.MakeMemberAccess(outerParam, p));
            }

            throw new Exception("Property not found!");
        }

        private static LambdaExpression GetResultSelector(Type outerType, Type innerType, Type resultType = null)
        {
            var outerParam = Expression.Parameter(outerType, "o");
            var innerParam = Expression.Parameter(innerType, "i");
            if (resultType == null)
                resultType = CreateResultType(outerType, innerType);

            var bindings = resultType.GetProperties().Select(x => BindToParam(x, outerParam, innerParam)).ToArray();
            var initializator = Expression.MemberInit(Expression.New(resultType), bindings);

            return Expression.Lambda(initializator, outerParam, innerParam);
        }

        public static IEnumerable InnerJoin(this IEnumerable source, IEnumerable inner, LambdaExpression outerKeySelector, LambdaExpression innerKeySelector, out Type resultSelectorType)
        {
            var innerType = inner.GetItemType();
            var outerType = source.GetItemType();

            if (innerType == null || outerType == null)
            {
                resultSelectorType = typeof(object);
                return (new object[0]).AsEnumerable();
            }

            var resultSelector = GetResultSelector(outerType, innerType);
            resultSelectorType = resultSelector.ReturnType;

            return Join(source, outerType, innerType, inner, outerKeySelector, innerKeySelector, resultSelector);
        }

        public static IEnumerable Join(this IEnumerable source, Type outerType, Type innerType, IEnumerable inner, LambdaExpression outerKeySelector, LambdaExpression innerKeySelector, LambdaExpression resultSelector)
        {
            var method = typeof(Enumerable).GetMethodInfo("Join", new[] { typeof(IEnumerable<>), typeof(IEnumerable<>), typeof(Func<,>), typeof(Func<,>), typeof(Func<,,>) });
            var outerKey = outerKeySelector.Compile();
            var innerKey = innerKeySelector.Compile();
            var resultObj = resultSelector.Compile();
            var methodInstance = method.MakeGenericMethod(outerType, innerType, innerKeySelector.ReturnType, resultSelector.ReturnType);
            var result = methodInstance.Invoke(null, new object[] { source, inner, outerKey, innerKey, resultObj }) as IEnumerable;

            return result;
        }
    }
}
