﻿using System;
using System.Linq.Expressions;

namespace Extensions
{
    public static class StringExtensions
    {
        public static MemberExpression ToMemberExpression(this string origin, ParameterExpression p, bool acceptNullableReturn)
        {
            var properties = origin.Split('.');

            var propertyType = p.Type;
            Expression propertyAccess = p;
            
            foreach (var prop in properties)
            {
                var property = propertyType.GetProperty(prop);
                propertyType = property.PropertyType;
                propertyAccess = Expression.MakeMemberAccess(propertyAccess, property);
            }

            if (!acceptNullableReturn && propertyType.IsNullable())
                propertyAccess = Expression.MakeMemberAccess(propertyAccess, propertyType.GetProperty("Value"));

            return propertyAccess as MemberExpression;
        }
    }
}
